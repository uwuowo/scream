# Adding new languages

Adding new languages is pretty easy. It involves deriving a single class and
setting the class attributes accordingly.

```python
class English(Language):
    optname = "en"
    name = "English"
    cols = 1
    choices = ["a", "A"] 
```

- Your new language has to derive Language.
- You need to set the option name, used in the command to select the language.
- You need to set the long name, for display purposes. Only seen using --debug.
- You need to set how many columns each character uses. See [[1]](#1).
- You need to set a list of characters for the script to scream with. See [[2]](#2).

### Is that it ?

That's it. The script uses the following dict comprehension to get each
Language subclass and fill out a map of option name to Language:

```python
SCREAMS = {k.optname: k() for k in Language.__subclasses__()}
```

---

<a id="1" href="#1">[1]</a>:
Wrapping at a specified column needs to be done on even columns, because
East Asian characters, with few exceptions, consume two columns.

This script doesn't count how many columns per character has been output. It
instead uses the column count as a step value for a range. It isn't technically
correct, but for the purposes of this script it works.

This does have the caveat that all choices for a language are assumed to have
the same width.

See [UAX #11: East Asian Width](https://www.unicode.org/reports/tr11/) for more
information.


<a id="2" href="#2">[2]</a>:
Technically they don't need to be single characters, they could be strings
of any length. The script assumes they're single characters.
