# Adding new patterns

Adding new patterns can be easy or hard depending on what you want it to do.

```python
class Iterate(Pattern):
    optname = "iterate"
    name = "Iterate"
    
    def __init__(self):
        self.index = -1
    
    def get(self, lang):
        self.index += 1
        if self.index > len(lang.choices) - 1:
            self.index = 0
        return self.index
```

- Your new pattern must derive Pattern.
- You need to set the option name, used in the command to select the pattern.
- You need to set the long name, for display purposes. Only seen using --debug.
- You need to define a `get` method that returns an index into the Language
  choices.

What state you need to maintain and/or how you choose an index to return is up
to you. You only need to ensure that it is within bounds of the choices list.

The above example will iterate over each character out of the choices list. It
will produce the following output:

```txt
> scream --pattern iterate 0 en 2> /dev/null | head -c 80
aAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaA

> scream --pattern iterate 0 jp 2> /dev/null | head -c 120
ぁあぁあぁあぁあぁあぁあぁあぁあぁあぁあぁあぁあぁあぁあぁあぁあぁあぁあぁあぁあ
```

### Is that it ?

That's it. The script uses the following dict comprehension to get each
Pattern subclass and fill out a map of option name to Pattern:

```python
PATTERNS = {k.optname: k() for k in Pattern.__subclasses__()}
```
