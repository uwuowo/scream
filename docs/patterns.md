# Patterns

This document shows example output for all available patterns. Each
example uses the following command as a template:

```bash
scream --pattern PATTERN 0 en 2> /dev/null | head -c 80
```

> Trying to do the above results in a broken pipe, which is why STDERR is being
> sent to /dev/null.

Sections will be named `<Pattern Name> (<option name>)`.

## Random (random)

```txt
> scream --pattern random 0 en 2> /dev/null | head -c 80
aAaaAaAaaAAaaAaaAaaAaAaaaaaaAAAAaAaAAAAaAAaaAAaAAAAAAAaAaaaAAAAAaAaAaaAaAaAaAaaa
```

## Iterate (iterate)

```txt
> scream --pattern iterate 0 en 2> /dev/null | head -c 80
aAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaA
```

## Repeat 5 (repeat5)

```txt
> scream --pattern repeat5 0 en 2> /dev/null | head -c 80
aaaaaAAAAAaaaaaAAAAAaaaaaAAAAAaaaaaAAAAAaaaaaAAAAAaaaaaAAAAAaaaaaAAAAAaaaaaAAAAA
```
