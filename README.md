[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

# scream

With this Python script, you can scream in your terminal.

I spent too much time on this.

## Installation

Drop the scream script anywhere accessible in your PATH and make it executable.

Example:

```bash
# Assumes you are in repository root and that ~/.local/bin is in PATH.
cp ./scream ~/.local/bin/scream
chmod 744 ~/.local/bin/scream
```

## How to use

```txt
usage: scream [-h] [--debug] [--pattern {random,repeat5}]
              delay [{en,jp}] [{nowrap,wrap}] [wrapcol]

Infinite screaming in your terminal.

positional arguments:
  delay                 Delay between scream outputs. Any positive decimal
                        value.
  {en,jp}               Scream language.
  {nowrap,wrap}         Whether the screaming should be constrained to a
                        single row.
  wrapcol               When to wrap the screaming back to the beginning, if
                        wrapping is enabled. Any positive integer value.

options:
  -h, --help            show this help message and exit
  --debug               Output final scream parameters.
  --pattern {random,repeat5}
                        Set scream pattern.
```

The only required argument is a delay in seconds. You may optionally specify
either English or Japanese, whether you want it to wrap over the same row or not
and when it should wrap.

Aside from options, all arguments are positional. You can't specify wrapcol
without specifying all previous arguments.

## Compatibility

Minimum compatible Python version is 3.5.

It was written for Linux but I don't believe there would be any issues running
it anywhere else.

## Dependencies

There are no external dependencies.

## Demonstration

This GIF demonstrates all the features of scream. There is a
[video version](./assets/demo.mp4) if you prefer that over GIF.

![Animation showing off the features of scream](./assets/demo.gif)

Also see [this document](./docs/patterns.md) to view examples of all available
patterns.

## Extensibility

[Adding new languages](./docs/adding-new-languages.md)

[Adding new patterns](./docs/adding-new-patterns.md)

## Known issues

This is meant to be a single portable-ish script. Adding a ton of languages and
patterns could make maintenance rather painful.

## License

The contents of this repository is licensed CNPLv7+.
